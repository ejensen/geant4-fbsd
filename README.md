# Fork of Geant4 for patching for FreeBSD

Port information available [here](https://www.freshports.org/science/geant4/).

Geant4 can be installed on FreeBSD in `Release` mode (without debug symbols) via pkg:

```shell
pkg ins geant4
```

Geant4 can be compiled and installed on FreeBSD in `Debug` mode (with debug symbols) via the ports tree:

```shell
cd /path/to/portstree/science/geant4
make WITH_DEBUG=1 install
```